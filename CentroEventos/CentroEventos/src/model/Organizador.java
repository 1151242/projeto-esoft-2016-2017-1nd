/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 * @author Bruno Alves <1151242@isep.ipp.pt>
 */
public class Organizador
{
    private Utilizador m_oUtilizador;
    
    /**
     * Construtor de instancia organizador
     */
    public Organizador() {
    }
    
    /**
     * Definir utilizador para organizador
     * @param u 
     */
    public void setUtilizador(Utilizador u) {
        this.m_oUtilizador = u;
    }
 
    /**
     * Valida organizador
     * @return 
     */
    private boolean check() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String toString() {
        return this.m_oUtilizador.toString();
    }
}
