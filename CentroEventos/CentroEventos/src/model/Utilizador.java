/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 * @author Bruno Alves <1151242@isep.ipp.pt>
 */
public class Utilizador {
    private String m_sNome;
    private String m_sEmail;
    private String m_sUserName;
    private String m_sPassword;
    
    public Utilizador() {
    }
    
    public Utilizador(String sNome, String sEmail) {
        this.m_sNome = sNome;
        this.m_sEmail = sEmail;
    }

    public void setNome(String nome) {
        this.m_sNome = nome;
    }

    public void setEmail(String email) {
        this.m_sEmail = email;
    }

    public void setUser(String user) {
        this.m_sUserName = user;
    }

    public void setPassword(String pwd) {
        this.m_sPassword = pwd;
    }

    public boolean validate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String toString() {
        return this.m_sNome + " - " + this.m_sEmail;
    }
}
