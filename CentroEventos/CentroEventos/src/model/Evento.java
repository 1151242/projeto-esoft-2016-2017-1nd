/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bruno Alves <1151242@isep.ipp.pt>
 */
public class Evento {

    private String m_titulo;
    private String m_descricao;
    private String m_dInicio, m_dFim;
    private String m_local;
    private String m_dInicioSub, m_dFimSub;
    private ListaOrganizadores m_lOrganizadores;
    private ListaFAEs m_lFAEs;
    private ListaCandidaturas m_lCandidaturas;
    private ListaAtribuicoes m_lAtribuicoes;
    
    public Evento() {
        m_lOrganizadores = new ListaOrganizadores();
        m_lFAEs = new ListaFAEs();
    }
    
    public void setTitulo(String titulo) {
        this.m_titulo = titulo;
    }
    
    public void setDescricao(String descricao) {
        this.m_descricao = descricao;
    }
    
    public void setPeriodo(String dInicio, String dFim) {
        this.setDataInicio(dInicio);
        this.setDataFim(dFim);
    }    
    
    private void setDataInicio(String dInicio) {
        this.m_dInicio = dInicio;
    }
    
    private void setDataFim(String dFim) {
        this.m_dFim = dFim;
    }
    
    public void setLocal(String local) {
        this.m_local = local;
    }
    
    public ListaOrganizadores getListaOrganizadores() {
        return this.m_lOrganizadores;
    }
    
    public boolean validate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.                
    }
    
    public ListaFAEs getListaFAEs() {
        return this.m_lFAEs;
    }
    
    public ListaAtribuicoes getListaAtribuicoes() {
        return this.m_lAtribuicoes;
    }
    
    public ListaCandidaturas getListaCandidaturas() {
        return this.m_lCandidaturas;
    }
}

