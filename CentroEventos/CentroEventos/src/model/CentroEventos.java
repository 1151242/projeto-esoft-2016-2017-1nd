/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 * @author Bruno Alves <1151242@isep.ipp.pt>
 */
public class CentroEventos
{
    private RegistoUtilizadores m_lUtilizadores;
    private RegistoEventos m_lEventos;
    private RegistoUtilizadoresNC m_lUtilizadoresNC;
    private RegistoAlgoritmos m_lAlgoritmos;
    
    public CentroEventos() {
        this.m_lUtilizadores = new RegistoUtilizadores();
        this.m_lEventos = new RegistoEventos();
        this.m_lUtilizadoresNC = new RegistoUtilizadoresNC();
        this.m_lAlgoritmos = new RegistoAlgoritmos();
    }
    
    public Evento newEvento() {
        return new Evento();
    }
    
    public RegistoEventos getRegistoEventos() {
        return this.m_lEventos;
    }
    
    public RegistoUtilizadores getRegistoUtilizadores() {
        return this.m_lUtilizadores;
    }
    
    public RegistoUtilizadoresNC getRegistoUtilizadoresNV() {
        return this.m_lUtilizadoresNC;
    }
    
}
