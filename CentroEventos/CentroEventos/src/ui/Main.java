/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import model.CentroEventos;

/**
 * changed and adapted by RSC mar.2017
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Main
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            CentroEventos ce = new CentroEventos();

            MenuUI uiMenu = new MenuUI(ce);

            uiMenu.run();
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
    
}
