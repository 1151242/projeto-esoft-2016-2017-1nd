/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;

import model.CentroEventos;
import utils.Utils;
import java.io.IOException;

/**
 * changed and adapted by RSC mar.2017
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class MenuUI
{
    private final CentroEventos m_ce;
    private String opcao;

    public MenuUI(CentroEventos ce)
    {
        m_ce = ce;
    }

    public void run() throws IOException
    {
        do
        {
            //opcao = "1";
            System.out.println("\n\n");
            System.out.println("1. Criar Evento");
            System.out.println("2. Definir FAE");
            System.out.println("3. Atribuir Candidatura");
            System.out.println("4. Decidir Candidaturas");
            System.out.println("5. Registar Candidatura");
            System.out.println("6. Registar Utilizador");
            System.out.println("7. Confirmar Registo de Utilizador");
            
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            switch(opcao)
            {
                case "1":
                    CriarEventoUI ui = new CriarEventoUI(m_ce);
                    ui.run();
                    break;
            }
        }
        while (!opcao.equals("0") );
    }
}
